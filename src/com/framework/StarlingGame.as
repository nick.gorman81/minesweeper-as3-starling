package com.framework
{
	import com.minesweeper.pages.*;
	
	import starling.display.Sprite;
	
	public class StarlingGame extends Sprite {
		
		private var pm:PageManager = PageManager.getInstance();
		
		// starling constructor class, just adds the pagemanager and creates our pages
		public function StarlingGame() {
			
			addChild(pm);
			
			pm.addPageByName(new TitleScreen(), 'titleScreen');
			pm.addPageByName(new GameGridScreen(), 'gameScreen');
			pm.addPageByName(new ResultsScreen(), 'resultsScreen');
			
			pm.navigateToPage('titleScreen');
		}
	}
}