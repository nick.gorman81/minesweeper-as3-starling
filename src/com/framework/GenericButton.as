package com.framework
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class GenericButton extends starling.display.Sprite {
		
		// generic button that draws a background texture and sets a label
		public function GenericButton(label:String) {	
			
			var rect:flash.display.Sprite = new flash.display.Sprite();
			rect.graphics.beginFill(0xcccccc);
			rect.graphics.drawRect(0,0,100,30);
			rect.graphics.endFill();
			var bmd:flash.display.BitmapData = new flash.display.BitmapData(rect.width, rect.height, true);
			bmd.draw(rect);
			addChild(new starling.display.Image(Texture.fromBitmapData(bmd, false, false)));
			
			var textField:TextField = new TextField(rect.width, rect.height, label);
			addChild(textField);
			
		}
		
	}
}