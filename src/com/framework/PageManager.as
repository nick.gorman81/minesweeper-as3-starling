package com.framework
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.minesweeper.pages.Page;
	import starling.display.Sprite;
	
	public class PageManager extends Sprite {
		
		private static var instance:PageManager = null;
		private var container:Sprite;
		private var currentScreen:String = '';
		private var inTransition:Boolean = false;
		
		// page manager is a singleton, controls navigation for application
		public static function getInstance() : PageManager {
			if ( PageManager.instance == null ) {
				PageManager.instance = new PageManager();
			}
			return PageManager.instance;
		}
		
		public function PageManager() {
			// add container which will hold all pages by name
			container = new Sprite();
			addChild(container);
		}
		
		// add pages to app during setup
		public function addPageByName(obj:Sprite, name:String):void {
			obj.name = name;
			container.addChild(obj);
			obj.visible = false;
		}
		
		// retuns page object
		public function getPageByName(name:String):Sprite {
			return (container.getChildByName(name)) as Sprite;
		}
		
		// navigate to page with transition
		public function navigateToPage(name:String):void {
			if ( name != currentScreen && inTransition == false ){
				
				currentScreen = name;
				inTransition = true;
				
				var page:Page = (container.getChildByName(name)) as Page;
				page.x = 600;
				container.setChildIndex(page, container.numChildren-1);
				page.visible = true;
				page.resetPage();
				TweenMax.to(page, .5, {x: 0, ease:Strong.easeOut, onComplete: hideOtherScreens });
			}
		}
		
		// run after transition has happened to hide subsequent pages and reset transition so pages dont get jammed up
		private function hideOtherScreens():void {
			for ( var i:int = 0; i < container.numChildren; i++ ){
				var page:Sprite = (container.getChildAt(i)) as Sprite;
				if ( page.name != currentScreen ){
					page.visible = false;
				}
			}
			inTransition = false;
		}
	}
}