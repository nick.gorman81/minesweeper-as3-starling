package com.minesweeper.pages {
	
	import com.framework.GenericButton;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class TitleScreen extends Page {
		
		public function TitleScreen() {
			
			// draw background using regular sprite and convert to texture		
			var rect:flash.display.Sprite = new flash.display.Sprite();
			rect.graphics.beginFill(0xffcc00);
			rect.graphics.drawRect(0,0,600,600);
			rect.graphics.endFill();
			var bmd:flash.display.BitmapData = new flash.display.BitmapData(rect.width, rect.height, true);
			bmd.draw(rect);
			bg = new starling.display.Image(Texture.fromBitmapData(bmd, false, false))
			addChild(bg);

			// add title label
			var titleText:TextField = new TextField(500, 40, "Welcome to MINESWEEPER!");
			titleText.y = 50;
			titleText.x = 50;
			titleText.fontSize = 30;
			addChild(titleText);
			
			// add instuctions label
			var instructions:TextField = new TextField(500, 200, "The purpose of the game is to open all the cells of the board which do not contain a bomb. You lose if you set off a bomb cell.\n\nEvery non-bomb cell you open will tell you the total number of bombs in the eight neighboring cells. Once you are sure that a cell contains a bomb, you can shift-click to put a flag it on it as a reminder. Once you have flagged all the bombs and uncovered all of the empty cells you are a winner!");
			instructions.y = 120;
			instructions.x = 50;
			instructions.fontSize = 13;
			addChild(instructions);
			
			// add difficulty label
			var subtitleText:TextField = new TextField(600, 20, "Choose your difficulty:");
			subtitleText.y = 350;
			addChild(subtitleText);
			
			// add button container
			var button_container:starling.display.Sprite = new starling.display.Sprite();
			addChild(button_container);
			
			var levels:Array = new Array('Easy','Medium','Difficult');
			
			// loop through difficulty levels and add buttons to the container
			for ( var i:int = 0; i < levels.length; i++ ){
				var button:GenericButton = new GenericButton(levels[i]);
				button.x = (button.width + 10)*i;
				button.name = levels[i].toLowerCase();
				button_container.addChild(button);
				button.addEventListener(TouchEvent.TOUCH, touchEvent);	
			}
			
			// center container
			button_container.y = 380;
			button_container.x = (600 - button_container.width)/2;
			

		}
		
		// manage touch events for the buttons and sets the difficulty based on the button name
		private function touchEvent(event:*):void {
			var touchEnd:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touchEnd != null) {
				
				pd.setDataForKey('difficulty',event.currentTarget.name);
				navigateToPage('gameScreen');
			}	
		}
	}
}