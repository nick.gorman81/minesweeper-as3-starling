package com.minesweeper.pages
{
	import com.minesweeper.gameutils.GameBoard;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	// game grid screen is the page that contains the game board
	public class GameGridScreen extends Page {
		
		private var gb:GameBoard;
		
		public function GameGridScreen() {
			
			// draw background using regular sprite and convert to texture		
			var rect:flash.display.Sprite = new flash.display.Sprite();
			rect.graphics.beginFill(0xffccff);
			rect.graphics.drawRect(0,0,600,600);
			rect.graphics.endFill();
			var bmd:flash.display.BitmapData = new flash.display.BitmapData(rect.width, rect.height, true);
			bmd.draw(rect);
			addChild( new starling.display.Image(Texture.fromBitmapData(bmd, false, false)));
			
			gb = new GameBoard();
			addChild(gb);
		}
		
		override public function resetPage():void{
			
			gb.initGame();
		}
		
	}
}