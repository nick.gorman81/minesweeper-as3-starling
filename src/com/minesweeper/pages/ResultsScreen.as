package com.minesweeper.pages
{

	import com.framework.GenericButton;
	import com.minesweeper.gameutils.Assets;
	
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	
	public class ResultsScreen extends Page {
		
		private var win:Image;
		private var lose:Image;
		private var titleText:TextField;
		
		// show results screen, after user has won or lost
		public function ResultsScreen() {
			
			lose = Assets.getInstance().getAssetByName('explosion');
			addChild(lose);
			
			win = Assets.getInstance().getAssetByName('heaven');
			addChild(win);
			
			// add title label
			titleText = new TextField(600, 40, "");
			titleText.y = 470;
			titleText.fontSize = 20;
			titleText.color = 0xffffff;
			addChild(titleText);
					
			var playAgain:GenericButton = new GenericButton('Play Again?');
			addChild(playAgain);
			playAgain.x = (600-playAgain.width)/2;
			playAgain.y = 520;
			playAgain.addEventListener(TouchEvent.TOUCH, touchEvent);
			
		}
		
		// determine if the player has won or lost and displays the appropriate message
		public function setStatus(didWin:Boolean):void{
			win.visible = false;
			lose.visible = false;
			if ( didWin ){
				win.visible = true;
				titleText.text = 'YOU WIN!';
			} else {
				lose.visible = true;
				titleText.text = 'YOU LOSE!';
			}
		}
		
		// if we play again, navigate to title screen so the user can choose their difficulty setting
		private function touchEvent(event:*):void {
			var touchEnd:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touchEnd != null) {
				navigateToPage('titleScreen');
			}	
		}
	}
}