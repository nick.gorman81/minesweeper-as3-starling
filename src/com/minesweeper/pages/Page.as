package com.minesweeper.pages
{
	import com.framework.PageManager;
	import com.minesweeper.gameutils.PlayerData;
	import com.minesweeper.gameutils.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	// base class for all pages
	public class Page extends Sprite {
		
		public var pm:PageManager = PageManager.getInstance();
		public var assets:Assets = Assets.getInstance();
		public var pd:PlayerData = PlayerData.getInstance();
		public var bg:Image;
		
		public function Page() {}
		
		public function navigateToPage(page:String):void {
			pm.navigateToPage(page);
		}
		
		public function resetPage():void{}
		
	}
}