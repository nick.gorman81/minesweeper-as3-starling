package com.minesweeper.gameutils {
	
	import com.minesweeper.gameutils.Assets;
	
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class GridSquare extends Sprite {
	
		private var assets:Assets = Assets.getInstance();
		
		public var isBomb:Boolean = false;
		public var isRevealed:Boolean = false;
		public var showFlag:Boolean = false;
		
		private var imgHidden:Image;
		private var imgUnlocked:Image;
		private var imgBomb:Image;
		private var imgFlag:Image;
		
		private var titleText:TextField;
		private var pt:Point;
		
		// setup the textures for the grid square
		public function GridSquare(x:int,y:int) {
			
			pt = new Point(x,y);
			
			imgHidden = Assets.getInstance().getAssetByName('hidden');
			addChild(imgHidden);
			
			imgUnlocked = Assets.getInstance().getAssetByName('unlocked');
			addChild(imgUnlocked);
			imgUnlocked.visible = false;
			
			imgBomb = Assets.getInstance().getAssetByName('bomb');
			addChild(imgBomb);
			imgBomb.visible = false;
			
			titleText = new TextField(imgHidden.width, imgHidden.height, "");
			addChild(titleText);
			titleText.fontSize = 20;
			titleText.visible = false;
			
			imgFlag = Assets.getInstance().getAssetByName('flag');
			addChild(imgFlag);
			imgFlag.visible = false;
			
		}
					
		// set the point value and set colors etc
		public function setPoints(val:int):void{
			
			// set value
			if ( val != 0 ){
				titleText.text = val.toString();	
			}
			
			// set color for value
			switch ( val ){
				case 1:
					titleText.color = 0x2f4cff;
					break;
				
				case 2:
					titleText.color = 0x0cc519;
					break;
				
				case 3:
					titleText.color = 0xe81919;
					break;
				
				default:
					titleText.color = 0x141670;
					break;
			}
		}
		
		// turn flag on or off and set bool
		public function setFlag():void {
			if ( showFlag == false ){
				showFlag = true;
				isRevealed = true;
				imgFlag.visible = true;
			} else {
				showFlag = false;
				isRevealed = false;
				imgFlag.visible = false;
			}
		}
		
		// return points
		public function getPoints():int {
			return Number(titleText.text);
		}
		
		// uncover on click
		public function uncover():void {
			isRevealed = true;
			imgHidden.visible = false;
			imgUnlocked.visible = true;
			if ( isBomb ) {
				imgBomb.visible = true;
			} else {
				titleText.visible = true;
			}
		}
	}
}