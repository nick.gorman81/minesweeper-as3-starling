package com.minesweeper.gameutils {
	
	import flash.utils.Dictionary;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class Assets extends Sprite {
		
		// this would normally be an extension to a texture atlas with sprite sheets
		// but since its a demo I'll just load them manually
		
		[Embed(source="../../../assets/grid_bomb.png")]
		public static const GridBomb:Class;
		
		[Embed(source="../../../assets/grid_hidden.png")]
		public static const GridHidden:Class;
		
		[Embed(source="../../../assets/grid_unlocked.png")]
		public static const GridUnlocked:Class;
		
		[Embed(source="../../../assets/grid_flag.png")]
		public static const GridFlag:Class;
		
		[Embed(source="../../../assets/explosion.png")]
		public static const Explosion:Class;
		
		[Embed(source="../../../assets/heaven.png")]
		public static const Heaven:Class;
		
		private var textureDictionary:Dictionary;
		
		public static function getInstance() : Assets {
			if ( Assets.instance == null ) {
				Assets.instance = new Assets();
			}
			return Assets.instance;
		}
		
		private static var instance:Assets = null;
		
		public function Assets() {
			textureDictionary = new Dictionary();
			textureDictionary['bomb'] = Texture.fromBitmap(new GridBomb());
			textureDictionary['hidden'] = Texture.fromBitmap(new GridHidden());
			textureDictionary['unlocked'] = Texture.fromBitmap(new GridUnlocked());
			textureDictionary['flag'] = Texture.fromBitmap(new GridFlag());
			textureDictionary['explosion'] = Texture.fromBitmap(new Explosion());
			textureDictionary['heaven'] = Texture.fromBitmap(new Heaven());
		}
		
		public function getAssetByName(name:String):Image {
			var image:Image = new Image(textureDictionary[name]);			
			return image;
		}
	}
}