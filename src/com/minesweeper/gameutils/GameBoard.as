package com.minesweeper.gameutils {
	
	import com.framework.PageManager;
	import com.minesweeper.gameutils.GridSquare;
	import com.minesweeper.pages.ResultsScreen;
	
	import starling.display.Sprite;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.HAlign;
	
	public class GameBoard extends Sprite {
		
		private var containerClip:Sprite;
		private var flagsRemaining:TextField;
		private var pd:PlayerData = PlayerData.getInstance();
		private var w:int;
		private var h:int;
		private var numOfMines:int;
		private var numOfFlags:int;
		private var clips:Array = [];
		private var isShiftPressed:Boolean = false;
		
		public function GameBoard() {
			
			// create container for board
			containerClip = new Sprite();
			addChild(containerClip);
			
			// add title label
			flagsRemaining = new TextField(200, 25, "");
			flagsRemaining.fontSize = 15;
			flagsRemaining.hAlign = HAlign.LEFT;
			addChild(flagsRemaining);
			
			// listen for shift so we can set flags
			this.addEventListener(KeyboardEvent.KEY_DOWN, reportKeyDown);
			this.addEventListener(KeyboardEvent.KEY_UP, reportKeyUp);
		}
		
		private function reportKeyDown(event:KeyboardEvent):void{
			if ( event.keyCode == 16 ){
				isShiftPressed = true;
			}
		}
		
		private function reportKeyUp(event:KeyboardEvent):void{
			if ( event.keyCode == 16 ){
				isShiftPressed = false;
			}
		}
		
		// dispose of all the cells before we start drawing a new set
		public function cleanUp():void{
			for ( var i:int = clips.length-1; i > -1; i-- ){
				clips[i].removeEventListener(TouchEvent.TOUCH, touchEvent);
				containerClip.removeChild(clips[i]);
			}
		}
		
		// the entry point for creating the grid
		public function initGame():void {
			cleanUp();
			var difficulty:String = pd.getDataForKey('difficulty');
			
			// calculate how big our grid is, and how many mines the board will have
			switch ( difficulty ){
				
				case 'easy':
					w = 10;
					h = 10;
					numOfMines = 20;
					break;
				
				case 'medium':
					w = 15;
					h = 15;
					numOfMines = 30;
					break;
				
				case 'difficult':
					w = 18;
					h = 18;
					numOfMines = 40;
					break;
			}
			
			numOfFlags = numOfMines;
			
			clips = new Array();
			
			// loop through totals and build grid
			for ( var i:int = 0; i < w; i++ ){
				for ( var j:int = 0; j < h; j++ ){
					var gs:GridSquare = new GridSquare(i,j);
					containerClip.addChild(gs);
					gs.x = gs.width * i;
					gs.y = gs.height * j;
					gs.name = 'grid_'+i+'_'+j;
					gs.addEventListener(TouchEvent.TOUCH, touchEvent);
					clips.push(gs);
				}
			}
			
			containerClip.x = (600 - containerClip.width)/2;
			containerClip.y = (600 - containerClip.height)/2;
			
			flagsRemaining.text = 'flags remaining: '+numOfFlags;
			flagsRemaining.x = containerClip.x;
			flagsRemaining.y = containerClip.y - flagsRemaining.height;
			
			// randomize our numberOfMines to the grid:
			setBombs(clips, numOfMines);
			
			
		}
		
		// randomly set bombs on the board once its been drawn
		private function setBombs(clips:Array, mines:int):void{
			var total:int = 0;
			do {
				var rnd:int = randomRange(0,clips.length-1);
				if ( clips[rnd].isBomb == false ){
					clips[rnd].isBomb = true;
					total++;
				}
			} while (total < mines);
			
			setScores();
			
		}
		
		// calculates the point value for each cell based on its 8 neighbours
		private function setScores():void{
			for ( var i:int = 0; i < w; i++ ){
				for ( var j:int = 0; j < h; j++ ){
					
					var gs:GridSquare = (containerClip.getChildByName('grid_'+i+'_'+j)) as GridSquare;
					
					var row:int = i;
					var col:int = j;
					
					var toCheck:Array = new Array( { x: row-1,y: col-1 }, { x: row,y: col-1}, { x: row+1,y: col-1}, { x: row-1,y: col}, { x: row+1,y: col}, { x: row-1,y: col+1}, { x: row,y: col+1}, { x: row+1,y: col+1});
					
					var score:int = 0;
					
					for ( var d:int = 0; d < toCheck.length; d++ ){
						
						if ( containerClip.getChildByName('grid_'+toCheck[d]['x']+'_'+toCheck[d]['y']) ){
							var square:GridSquare = (containerClip.getChildByName('grid_'+toCheck[d]['x']+'_'+toCheck[d]['y'])) as GridSquare;
							if ( square.isBomb ){
								score++;
							}
						} 
					}
					
					gs.setPoints(score);
				}
			}
		}
		
		private function randomRange(minNum:Number, maxNum:Number):Number {
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		// when clicking on an open tile, this will search neighbours for other 0 tiles, so we can open up the space recursively
		private function searchRadiusForOpenSpace(xPos:int, yPos:int):void{
			var row:int = xPos;
			var col:int = yPos;
			
			var toCheck:Array = new Array( { x: row-1,y: col-1 }, { x: row,y: col-1}, { x: row+1,y: col-1}, { x: row-1,y: col}, { x: row+1,y: col}, { x: row-1,y: col+1}, { x: row,y: col+1}, { x: row+1,y: col+1});
			
			for ( var d:int = 0; d < toCheck.length; d++ ){
				
				if ( containerClip.getChildByName('grid_'+toCheck[d]['x']+'_'+toCheck[d]['y']) ){
					
					var square:GridSquare = (containerClip.getChildByName('grid_'+toCheck[d]['x']+'_'+toCheck[d]['y'])) as GridSquare;
					
					if ( square.isRevealed == false ){
						if ( square.getPoints() == 0 && square.isBomb == false){
							square.uncover();
							searchRadiusForOpenSpace(toCheck[d]['x'], toCheck[d]['y']);
						} else if ( square.getPoints() > 0 && square.isBomb == false ) {
							square.uncover();
						}
					}

				} 
			}
		}
		
		// remove all touch events from cells
		private function disableAllButtons():void {
			for ( var i:int = clips.length-1; i > -1; i-- ){
				clips[i].removeEventListener(TouchEvent.TOUCH, touchEvent);
			}
		}
		
		private function touchEvent(event:*):void {
			var touchEnd:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touchEnd != null) {
							
				// checks to see if the name begins with grid, assuming its a grid cell
				if ( event.currentTarget.name.substr(0,4) == 'grid' ){
					
					var name:Array = event.currentTarget.name.split('_');
					var xPos:int = name[1];
					var yPos:int = name[2];
					var gs:GridSquare = GridSquare(containerClip.getChildByName('grid_'+xPos+'_'+yPos));
					
					// check to see if we're adding or removing a flag
					if ( isShiftPressed ) {
						
						if ( gs.showFlag == false ){
							if ( numOfFlags > 0 ){
								numOfFlags--;	
								gs.setFlag();
							}
						} else {
							numOfFlags++;
							gs.setFlag();
						}
						
						flagsRemaining.text = 'flags remaining: '+numOfFlags;
						
					// this means we're uncovering a block
					} else {
					
						gs.uncover();
						
						// check to see if the bomb boolean is set, game over
						if ( gs.isBomb ){
							disableAllButtons();
							var results:ResultsScreen = ResultsScreen(PageManager.getInstance().getPageByName('resultsScreen'));
							results.setStatus(false);
							PageManager.getInstance().navigateToPage('resultsScreen');	
						
						// this means we're ok, lets check to see if its an open square, if it is, lets check neighbours recursively
						} else {
							if ( gs.getPoints() == 0 ){
								// open up the 0's and expose the closest neighbour
								searchRadiusForOpenSpace(xPos,yPos);
							} 
						}
					}
					
					// check for flags to be 0 and left over isRevealed
					checkIfPlayerWon();
				}
			}	
		}
		
		// the player would technically win of all cells are revealed, when you set a flag on a cell, it marks the cell as revealed
		private function checkIfPlayerWon():void{
			
			// calculate total cells
			var totalCells:int = w * h;
			
			// subtract 1 for each cell that is revealed
			for ( var i:int = 0; i < clips.length; i++ ){
				if ( clips[i].isRevealed == true ){
					totalCells--;
				}
			}
			
			// if all cells are revealed, you win!
			if ( totalCells == 0 ){
				disableAllButtons();
				var results:ResultsScreen = ResultsScreen(PageManager.getInstance().getPageByName('resultsScreen'));
				results.setStatus(true);
				PageManager.getInstance().navigateToPage('resultsScreen');	
			}
		}
		
	}
}