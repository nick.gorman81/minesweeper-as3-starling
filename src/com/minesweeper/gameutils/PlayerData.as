package com.minesweeper.gameutils
{
	import flash.utils.Dictionary;
	import starling.display.Sprite;
	
	public class PlayerData extends Sprite {
		
		private static var instance:PlayerData = null;
		private var dictionary:Dictionary;
		
		public static function getInstance() : PlayerData {
			if ( PlayerData.instance == null ) {
				PlayerData.instance = new PlayerData();
			}
			return PlayerData.instance;
		}
		
		// this is the class that would hold all of the users custom data, such as name, score, difficulty, etc.
		public function PlayerData() {
			dictionary = new Dictionary();
		}
		
		public function setDataForKey(key:String, obj:*):void{
			dictionary[key] = obj;
		}
		
		public function getDataForKey(key:String):* {
			return dictionary[key];
		}

	}
}