package
{
	import com.framework.StarlingGame;
	
	import flash.display.Sprite;
	
	import starling.core.Starling;
	
	[SWF(width="600", height="600", frameRate="60", backgroundColor="#ffffff")]
	
	public class Minesweeper extends Sprite {
		
		private var _starling:Starling;
		
		// constructor, adding starling instance
		public function Minesweeper() {
			
			_starling = new Starling(StarlingGame, stage);
			_starling.start();
			
		}
	}
}